<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>TP - Cybersécurité</title>
    <meta property="og:image" content="https://discord.com/assets/ee7c382d9257652a88c8f7b7f22a994d.png" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

<style>
    body{
        width : 100%;
        height: 100%;
    }
    img{
        margin-top: 0;
        position: absolute;
        width : 100%;
        height: 100%;
    }
    .card{
        width: 90%;
        left:5%;
    }
</style>
</head>
<body>
<img src="https://www.presse-citron.net/app/uploads/2020/12/discord.jpg" alt="discord">
<div class="card text-center">
    <div class="card-header">
        TP - CYBERSECURITE
    </div>
    <div class="card-body">
        <h5 class="card-title">Participant du TP : </h5>
        <p>LOUIS LE POGAM</p>
        <p>KILLIAN BELLOUR</p>
        <a href="callback.php">
            <button type="button" class="btn btn-primary">Je me connecte à discord !</button>
        </a>

    </div>
    <div class="card-footer text-muted">
        Copyright 2021
    </div>
</div>

</body>
</html>
