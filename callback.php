<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>TP - Cybersécurité</title>
    <meta property="og:image" content="https://discord.com/assets/ee7c382d9257652a88c8f7b7f22a994d.png" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <style>
        body{
            width : 100%;
            height: 100%;
        }
        img{
            z-index: -10000;
            margin-top: 0;
            position: absolute;
            width : 100%;
            height: 100%;
        }

    </style>
</head>
<body>
<img src="https://www.presse-citron.net/app/uploads/2020/12/discord.jpg" alt="discord">

<?php

require __DIR__ . '/vendor/autoload.php';

session_start();

$provider = new \Wohali\OAuth2\Client\Provider\Discord([
    'clientId' => '858814378396614677',
    'clientSecret' => 'O3BsP_ziHvgEYcn9u_dhQkSMSL9sJOBG',
    'redirectUri' => 'http://localhost/tp_discord_oauth2/callback.php'
]);

if (!isset($_GET['code'])) {

    // Step 1. Get authorization code
    $authUrl = $provider->getAuthorizationUrl();
    $_SESSION['oauth2state'] = $provider->getState();
    header('Location: ' . $authUrl);
    die();

// Check given state against previously stored one to mitigate CSRF attack
} elseif (empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth2state'])) {

    unset($_SESSION['oauth2state']);
    exit('Invalid state');

} else {

    // Step 2. Get an access token using the provided authorization code
    $token = $provider->getAccessToken('authorization_code', [
        'code' => $_GET['code']
    ]);

    $_SESSION['token'] = $token->getToken();
    // Show some token details
    echo '<h2>Token details:</h2>';
    echo 'Ton token: ' . $token->getToken() . "<br/>";
    echo 'Ton refresh token: ' . $token->getRefreshToken() . "<br/>";
    echo 'Ton token est ';
    echo ($token->hasExpired() ? 'expiré' : 'encore actif') . "<br/>";

    // Step 3. (Optional) Look up the user's profile with the provided token
    try {

        $user = $provider->getResourceOwner($token);

        echo '<h2>Voici quelque informations sur toi ! </h2>';
        printf('Salut %s#%s!<br/><br/>', $user->getUsername(), $user->getDiscriminator());
        $user = $user->toArray();
        printf('Ton id sur discord est : ' . $user['id'].'<br><br>');
        printf('Ton email est : ' . $user['email'].'<br><br>');
        printf('Ton avatar est : ' . $user['avatar'].'<br><br>');

    } catch (Exception $e) {

        // Failed to get user details
        exit('Oh dear...' . $e->getMessage());

    }
}
?>

</body>
</html>

